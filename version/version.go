package version

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/bkc/changelogger/entry"
)

type Version struct {
	Breaking    []*entry.Entry
	Feature     []*entry.Entry
	Bugfix      []*entry.Entry
	Enhancement []*entry.Entry
	Misc        []*entry.Entry
}

func (c *Version) Populate(skipDir string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil || info.IsDir() || strings.HasPrefix(path, skipDir) || !strings.HasSuffix(path, ".yml") {
			return err
		}
		f, err := os.Open(path)
		if err != nil {
			return err
		}
		defer f.Close()
		cle, err := entry.LoadEntry(f)
		if err != nil {
			return fmt.Errorf("failed to load changelog entry: %v", err)
		}
		switch cle.Kind {
		case "breaking":
			c.Breaking = append(c.Breaking, cle)
		case "feature":
			c.Feature = append(c.Feature, cle)
		case "bugfix":
			c.Bugfix = append(c.Bugfix, cle)
		case "enhancement":
			c.Enhancement = append(c.Enhancement, cle)
		default:
			c.Misc = append(c.Misc, cle)
		}
		return nil
	}
}

func (c *Version) Write(f io.Writer) error {
	if len(c.Breaking) > 0 {
		f.Write([]byte("* BREAKING\n"))
		for _, cle := range c.Breaking {
			cle.Write(f)
		}
	}
	if len(c.Bugfix) > 0 {
		f.Write([]byte("* BUGFIXES\n"))
		for _, cle := range c.Bugfix {
			cle.Write(f)
		}
	}
	if len(c.Feature) > 0 {
		f.Write([]byte("* FEATURES\n"))
		for _, cle := range c.Feature {
			cle.Write(f)
		}
	}
	if len(c.Enhancement) > 0 {
		f.Write([]byte("* ENHANCEMENT\n"))
		for _, cle := range c.Enhancement {
			cle.Write(f)
		}
	}
	if len(c.Misc) > 0 {
		f.Write([]byte("* MISC\n"))
		for _, cle := range c.Misc {
			cle.Write(f)
		}
	}

	return nil
}
