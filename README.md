# Changelogger

Changelogs make stupidly easy.

**Note: Extremly unefficient since it re-builds the changelog on each run...**

## How?

You store your changelog entries in `changelog/unreleased`. When you want to
do a release you run `changelogger -version 1.0.0` and `changelog/v1.0.0` will
be populated, and CHANGELOG.md will be re-written.

## Why?

Mainly because I wanted a single-binary thing I could just throw into a 
repo and it just works. Hugely inspired by GitLabs
[Changelog Process](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/changelog.md)  
Real reason for writting it? I needed it for (Gitea)[https://github.com/go-gitea/gitea/issues/505]

## Installing

$ `go get gitlab.com/bkc/changelogger/cmd/changelogger`

No releases yet as I'm not pleased with it as-is.

## Options
```
 -changelogDir [path]   Where to read changelogs from. Defaults "changelog"
 -version      [string] What version are we generating. Required
 -outFile      [file]   What file to write the changelog to. Default: "CHANGELOG.md"
 -force        [bool]   Force update the changelog even if it exists. DO NOT USE!
```

## Format

|Name|Type|Required|Description|
|----|----|--------|-----------|
|kind|string|yes|What kind of entry this is. See [more below](#kind)|
|title|string|yes|The title of the Entry|
|pull_request|int|yes|Which Pull-Request this was introduced in|
|author|string|no|Who made this. (plain string so anything goes)|
|issue|int|no|Which Issue this relates to|

## Kind

Changelogger defines 4 kinds of entries, `breaking`, `feature`, `bugfix`,
`enhancement`. They will be sorted in that other when written to changelog.
Everything else will end up under `misc`.

## Future/TODO

- Don't rewrite every time, try to do some simple parsing to see where the new
  parts goes
- GitHub/GitLab flag so it handles PR/MR correctly :trollface:
- Define your own "kinds", in that order (is gonna be PITA :unamused: )
- Config-file maybe...
