package entry

import (
	"fmt"
	"io"
	"io/ioutil"

	yaml "gopkg.in/yaml.v2"
)

const prUrlFormat = ""

type Entry struct {
	Kind        string  `yaml:"kind"`
	Title       string  `yaml:"title"`
	PullRequest int     `yaml:"pull_request"`
	Issue       *int    `yaml:"issue"`
	Author      *string `yaml:"author"`
}

func LoadEntry(r io.Reader) (*Entry, error) {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, fmt.Errorf("can't read changelog entry into memory: %v", err)
	}

	newCL := &Entry{}
	if err := yaml.Unmarshal(data, newCL); err != nil {
		return nil, fmt.Errorf("can't unmarshal changelog entry: %v", err)
	}

	return newCL, nil
}

func (c *Entry) Write(w io.Writer) (int, error) {
	if len(prUrlFormat) == 0 {
		return fmt.Fprintf(w, "  * %s %d\n", c.Title, c.PullRequest)
	}
	url := fmt.Sprintf(prUrlFormat, c.PullRequest)
	str := fmt.Sprintf("  * %s [%d](%s)\n", c.Title, c.PullRequest, url)
	return w.Write([]byte(str))
}
